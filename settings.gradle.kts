pluginManagement {
    repositories {
        mavenCentral()
        mavenLocal()
        gradlePluginPortal()
        google()
    }
}

rootProject.name = "edgar-memoizer-sender"
